namespace Todoer.Models;

public class Todo {
  public int ID { get; set; }
  public string Description { get; set; }
  public DateTime DueBy { get; set; }
  public bool Completed { get; set; }
}