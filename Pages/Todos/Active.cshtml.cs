using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Todoer.Models;

namespace Todoer.Pages.Todos
{
    public class ActiveModel : PageModel
    {
        private readonly TodoerDbContext _context;

        public ActiveModel(TodoerDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Todo> Todo { get;set; }

        public async Task OnGetAsync()
        {
            Todo = from todo in _context.Todo where todo.Completed == false select todo;
        }
    }
}
